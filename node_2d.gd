extends Node2D

var time := 0.0

var last_refreshed := -100000000.0
var interval := 5.0

var texts := [
	["x=x'", 250],
	["class körte : public alma", 130],
	["public static void main(String[] args)", 95],
	["P É K", 250],
	["megyünk csoportos terápiára", 120],
	["https://people.inf.elte.hu/gt/gt.jpg", 100],
	["if(enor.status == abnorm) exit(1);", 105],
	["this->zsák.put_in(your_mouth);", 90],
	["#include <gtlib.hpp>", 155],
	["class MaxSearch : public Procedure<Item>", 85],
	["Négyszög:Sokszög | csúcsok = < A, B, C, D >", 85],
	["CheckBoxBakery.getInstance().getNewCheckboxBakery().bake()", 70],
	["l := hamis; t := 0; e = ε", 140],
	["dokumentáció kész?", 170],
	["Horgász-->oHal", 190],
	["aggregáció. kompozíció. asszociáció.", 100],
	["+ priority() : int {virtual, query}", 110],
	["singleton = best practice", 130],
	["SingletonEntity::destroy();", 130]
]

var texts_left := []

func _process(_delta):
	time += _delta
	
	if len(texts_left) == 0:
		for text in texts:
			texts_left.append(text)
		texts_left.shuffle()
	
	if Input.is_action_just_pressed("ui_exit"):
		get_tree().quit()
	#if Input.is_action_just_pressed("space"):
	#	new_text()
	
	if time > last_refreshed+interval:
		last_refreshed = time
		new_text()

func new_text():
	var igazsag :Array= texts_left.pop_back()
	$igazsag.text = igazsag[0]
	$igazsag.add_theme_font_size_override("asd", igazsag[1])
	var dynamic_font = preload("res://BebasNeue-Regular.ttf")
	dynamic_font.fixed_size = igazsag[1]
	$igazsag.add_theme_font_override("font", dynamic_font)
